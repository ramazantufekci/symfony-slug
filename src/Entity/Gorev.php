<?php

namespace App\Entity;
use App\EntityListener\GorevEntityListener;
use App\Repository\GorevRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\String\Slugger\SluggerInterface;
/**
 * @ORM\Entity(repositoryClass=GorevRepository::class)
 * #ORM\EntityListeners({"App\EntityListener\GorevEntityListener"})
 * @UniqueEntity("slug")
 */
class Gorev
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $slug;
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function computeSlug(SluggerInterface $slugger)
    {
	    
	    $this->slug = self::getName();	    
	    if(!empty($this->slug) )//|| '-' === $this->slug)
	    {
		    //$repo = new GorevRepository(Gorev::class);
		    //var_dump($repo);exit();
		    $this->slug = (string) $slugger->slug((string) $this->slug)->lower();
	    }
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
