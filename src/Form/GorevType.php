<?php

namespace App\Form;

use App\Entity\Gorev;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GorevType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,['attr'=>['class'=>'form-control']])
	    ->add('description',TextType::class,['label'=>'Description:','attr'=>['class'=>'form-control']])
//    ->add('slug',TextType::class,['attr'=>['class'=>'form-control','data-id'=>1]])
    ->add('submit',SubmitType::class,['attr'=>['class'=>'btn btn-primary mt-3']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
		'data_class' => Gorev::class,
		'attr'=>['id'=>'new-gorev-form'],
        ]);
    }
}
