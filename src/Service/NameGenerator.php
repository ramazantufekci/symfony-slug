<?php
namespace App\Service;


class NameGenerator
{
	public function randomName()
	{
		$names = [
			'Ramazan',
			'Derya',
			'Nehir',
		];
		$index = array_rand($names);
		return $names[$index];
	}
}
