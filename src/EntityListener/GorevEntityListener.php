<?php
namespace App\EntityListener;

use App\Entity\Gorev;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\String\Slugger\SluggerInterface;
use App\Repository\GorevRepository;

class GorevEntityListener
{
	private $slugger;

	public function __construct(SluggerInterface $slugger)
	{
		$this->slugger = $slugger;
	}

	public function prePersist(Gorev $gorev, LifecycleEventArgs $event)
	{
		$gorev->computeSlug($this->slugger);
	}

	public function preUpdate(Gorev $gorev, LifecycleEventArgs $event)
	{
		$gorev->computeSlug($this->slugger);
	}

}
