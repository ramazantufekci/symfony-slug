<?php

namespace App;
use Symfony\Contracts\EventDispatcher\Event;
final class DeryaEvents extends Event
{
	const KAYDEDILDI = 'siparis.kaydedildi';
	private $urun;
	public function __construct(string $urun)
	{
		$this->urun = $urun;
	}

	public function getUrun():string
	{
		return $this->urun;
	}
}
