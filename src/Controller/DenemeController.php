<?php

namespace App\Controller;
use App\DeryaEvents;
use App\Form\GorevType;
use App\Entity\Gorev;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Service\NameGenerator;
class DenemeController extends AbstractController
{
    /**
     * @Route("/admin/gorev", name="deneme")
     */
    public function index(EventDispatcherInterface $dispatcher,Request $request): Response
    {

	    $urun = "kayısı";
	    $gorev = new Gorev();
	    //$user = new User();
	    //$user->setRoles = ["ROLE_ADMIN"];
	    //$er = $this->getDoctrine()->getManager();
	    //$user = $er->getRepository(User::class)->find(1);
	    //$user->setRoles(array('ROLE_ADMIN'));
	    //$er->flush();
	    $dispatcher->dispatch(new DeryaEvents($urun),DeryaEvents::KAYDEDILDI);
	    $form = $this->createForm(GorevType::class,$gorev);
	    $form->handleRequest($request);
	    if($form->isSubmitted() && $form->isValid())
	    {
		    $gorev = $form->getData();
		    //var_dump($form);exit();
		    $em = $this->getDoctrine()->getManager();
		    $em->persist($gorev);
		    $em->flush();
		    return $this->redirectToRoute('deneme_mal');
	    }
	    $em2 = $this->getDoctrine()->getManager();
	    $repo = $em2->getRepository(Gorev::class)->findBy(array(),['id'=>'DESC']);
	    //var_dump($repo);
	    return $this->render('deneme/index.html.twig',[
		    'form'=>$form->createView(),
		    'gorevler'=>$repo,
	    ]);
    }

    /**
     * @Route("/mal", name="deneme_mal")
     * @param NameGenerator $nameGenerator
     */

    public function Isim(NameGenerator $nameGenerator)
    {
	$nam = $nameGenerator->randomName();
	return new Response($nam);
    }

    /**
     * @Route("/admin", name="deneme_admin")
     */

    public function admin()
    {
	    return $this->render("deneme/admin.html.twig");
    }

    /**
     * @Route("/", name="deneme_home")
     */
    public function home()
    {
	    return $this->render("deneme/home.html.twig");
    }
}
